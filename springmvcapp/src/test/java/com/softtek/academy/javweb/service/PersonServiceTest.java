package com.softtek.academy.javweb.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.configuration.JDBCConfiguration;
import com.softtek.academy.javaweb.service.PersonService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JDBCConfiguration.class })
@WebAppConfiguration
public class PersonServiceTest {

    @Autowired
    private PersonService studentService;
    @Test
    public void getByIdTest(){
   
       
        Person expectedPerson = new Person("neto gonzalez", "25", 2);
        int id = 2;

        Person actualPerson =  studentService.getById(id);
       
        assertNotNull(actualPerson);
        assertEquals(expectedPerson.getName(), actualPerson.getName());
    }
    
}