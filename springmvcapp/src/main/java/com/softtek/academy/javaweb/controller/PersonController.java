package com.softtek.academy.javaweb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.service.PersonService;


@Controller
public class PersonController {
	
	@Autowired
	PersonService personService;
	
	
	 @RequestMapping(value="/listperson", method = RequestMethod.GET)
	    public ModelAndView getMethodName() {
	        List<Person> list = personService.getAll();
	        ModelAndView page = new ModelAndView("list");
	        page.addObject("list", list);
	        return page;
	    }
	    

	    @RequestMapping(value = "/savePerson", method = RequestMethod.POST)
	    public ModelAndView addPerson(@ModelAttribute("index") Person person, ModelMap model) {
	        model.addAttribute("name", person.getName());
	        model.addAttribute("age", person.getAge());
	        model.addAttribute("id", person.getId());
	        personService.saveUser(person);
	        List<Person> list = personService.getAll();
	        ModelAndView page = new ModelAndView("list");
	        page.addObject("list", list);
	        return page;
	    }

	    @RequestMapping(value = "/updateForm/{id}", method = RequestMethod.GET)
	    public ModelAndView update(@PathVariable(value="id") int id) {
	        Person person = personService.getById(id);
	        return new ModelAndView("updateForm", "update", person);
	    }

	    @RequestMapping(value = "/updatePerson", method = RequestMethod.POST)
	    public ModelAndView updatePerson(@ModelAttribute("person") Person person, ModelMap model) {
	        model.addAttribute("name", person.getName());
	        model.addAttribute("age", person.getAge());
	        model.addAttribute("id", person.getId());
	        personService.update(person.getId(), person);
	        List<Person> list = personService.getAll();
	        ModelAndView page = new ModelAndView("list");
	        page.addObject("list", list);
	        return page;
	    }

	    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	    public ModelAndView getMethodName(@PathVariable(value="id") int id) {
	    	personService.delete(id);
	       List<Person> list = personService.getAll();
	       ModelAndView page = new ModelAndView("list");
	       page.addObject("list", list);
	       return page;
	    }
	
	
	    
	    
}
