package com.softtek.academy.javaweb.service;

import java.util.List;

import com.softtek.academy.javaweb.beans.Person;



public interface PersonService {

	  	List<Person> getAll();
	    Person getById(int id);
	    void delete(int id);
	    void update(int id, Person person);
	    void saveUser(Person person);
}
