package com.softtek.academy.javaweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.softtek.academy.javaweb.beans.Person;


@Repository("personRepository")
public class PersonRepository implements PersonDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Person> getAll() {
        return (List<Person>) entityManager.createQuery("SELECT p FROM Person p", Person.class).getResultList();
    }

    @Override
    public Person getById(int id) {

        return entityManager.find(Person.class, id);
    }

    @Override
    public void delete(int id) {
        Person p = entityManager.find(Person.class, id);
        entityManager.remove(p);
    }

    @Override
    public void update(int id, Person person) {
        Person p = entityManager.find(Person.class, id);
        p.setName(person.getName());
        p.setAge(person.getAge());
    }

    @Override
    public void saveUser(Person person) {
        entityManager.persist(person);
    }
}
