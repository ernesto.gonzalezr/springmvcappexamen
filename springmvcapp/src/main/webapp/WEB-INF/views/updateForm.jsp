<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form" %>
    
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Modificar Alumno</title>
	</head>
	<body>
		<h2>Informacion del alumno</h2>
		<form:form method="POST" action ="/SpringMVCApp/updatePerson" modelAttribute="update">
			<table>
				<tr>
					<td> <form:label path = "name">Nombre: </form:label></td>
					<td> <form:input path = "name"></form:input></td>
				</tr>
				<tr>
					<td> <form:label path = "age">Edad: </form:label></td>
					<td> <form:input path = "age"></form:input></td>
				</tr>
				<tr>
					<td> <form:label path = "id">Id: </form:label></td>
					<td> <form:input path = "id"></form:input></td>
				</tr>
				<tr>
					<td>
						<input type = "submit" value = "Submit"/>
					</td>
				</tr>
			</table>
		</form:form>

		<a href="/SpringMVCApp">Menu</a>
	</body>
</html>