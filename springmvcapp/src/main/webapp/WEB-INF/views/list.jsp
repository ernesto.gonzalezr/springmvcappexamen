<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="ISO-8859-1">
    <title>Lista de Estudiantes</title>
    <style>
table, th, td {
  border: 1px solid black;
}
</style>
</head>

<body>
    <table>
        <thead>
            <tr>ID</tr>
            <tr>Nombre</tr>
            <tr>Edad</tr>
            <tr>Eliminar</tr>
             <tr>Modificar</tr>
        </thead>

        <tbody>
            <c:forEach items="${list}" var="person">
            <tr>
                <td>${person.getId()}</td>
                <td>${person.getName()}</td>
                <td>${person.getAge()}</td>
                <td><a href="/SpringMVCApp/delete/${person.getId()}">Eliminar</a></td>
                <td><a href="/SpringMVCApp/updateForm/${person.getId()}">Modificar</a></td>
            </tr>
            </c:forEach>
        </tbody>
    </table>
    
    <a href="/SpringMVCApp">Menu</a>
</body>

</html>